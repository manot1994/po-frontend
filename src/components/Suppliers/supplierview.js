import React from 'react'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import { BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Profileimg from '../assets/profile_image.jpeg'
import TextField from '@material-ui/core/TextField';
import { TextareaAutosize } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import '../Suppliers/Supplierstyle.css'
import Addproducts from '../Suppliers/addproducts'


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: "#000",
        fontSize:'16px'
    },
}));
export default function Supplierview() {
    const classes = useStyles();

    return (
        <div>
            <Paper>
                <Grid container spacing={3} >

                    <Grid item md={6} xs={6} className="breadcrumbs">
                        <Breadcrumbs aria-label="breadcrumb">
                            <Link color="inherit" href="/" >Simple Purchase Orders</Link>
                            {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
                            <Typography color="textPrimary">Welcome</Typography>
                        </Breadcrumbs>
                    </Grid>
                    <Grid item md={6} xs={6} className="breadcrumbs1">
                        <Addproducts />
                        <Link to="/Products" className="router-link"> <Button size="small" variant="contained" color="primary">VIEW SUPPLIER PRODUCTS</Button></Link>
                    </Grid>
                </Grid>
            </Paper>
            <Grid container spacing={2} style={{ marginTop: '25px' }}>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                    <Grid container>
                        <Grid item md={10}><img src={Profileimg} style={{ width: '125px' }}></img></Grid>
                        <Grid item md={2}><Button variant="contained" size="small">Edit</Button></Grid>
                        </Grid>
                        <TextField id="standard-full-width" label="Supplier Name" style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-full-width"
                            label="Supplier Code"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-full-width"
                            label="Contact Number"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-full-width"
                            label="Mail Id"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-full-width"
                            label="Pan No"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-full-width"
                            label="GST IN"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <Button variant="contained" color="primary" style={{width:'100%'}}>Save Changes</Button>
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                        <div className="supplierview-heading">Bank Account Details</div>
                        <TextField
                            id="standard-full-width"
                            label="Account Number"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-full-width"
                            label="Bank Name"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-full-width"
                            label="Branch Name"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        /><TextField
                            id="standard-full-width"
                            label="IFSC Code"
                            style={{ margin: 8 }}
                            defaultValue="CNRB0000901"
                            fullWidth
                            margin="normal"
                            InputProps={{
                                readOnly: true,
                              }}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    <Button variant="contained" color="primary" style={{width:'100%'}}>Save Changes</Button>
                    </Paper>
                    <Paper className={classes.paper} style={{ marginTop: '10px' }}>
                    <div className="supplierview-heading">Bank Account Details</div>
                        <InputLabel style={{ textAlign: 'left', padding: '8px', fontSize: '13px' }}>Address</InputLabel>
                        <TextareaAutosize style={{ width: '100%', height: '80px' }}
                             />
                        <TextField
                            id="standard-full-width"
                            label="City"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="standard-full-width"
                            label="State"
                            style={{ margin: 8 }}
                            
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        /><TextField
                            id="standard-full-width"
                            label="Pincode"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                     <Button variant="contained" color="primary" style={{width:'100%'}}>Save Changes</Button>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}
