import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { InputLabel } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import '../Suppliers/Supplierstyle.css'

const top100Films = [
    { title: 'Samsung M31'},
    { title: 'The Godfather'},
    { title: 'The Godfather: Part II'},
    { title: 'The Dark Knight'},
    { title: '12 Angry Men'},
    { title: "Schindler's List"},
    { title: 'Pulp Fiction'},
    { title: 'The Lord of the Rings: The Return of the King'},
    { title: 'The Good, the Bad and the Ugly'},
    { title: 'Fight Club'},
    { title: 'The Lord of the Rings: The Fellowship of the Ring'},
    { title: 'Star Wars: Episode V - The Empire Strikes Back'},
    { title: 'Forrest Gump'},
    { title: 'Inception'},
    { title: 'The Lord of the Rings: The Two Towers'},
    { title: "One Flew Over the Cuckoo's Nest"},
  ];
export default function ScrollDialog() {
  const [open, setOpen] = React.useState(false);
  const [scroll, setScroll] = React.useState('paper');

  const handleClickOpen = (scrollType) => () => {
    setOpen(true);
    setScroll(scrollType);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);

  return (
    <div>
      <Button onClick={handleClickOpen('paper')} color="primary" variant="contained" size="small">Add Products</Button>
      <Dialog
        open={open}
        onClose={handleClose}
        scroll={scroll}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">Add Products</DialogTitle>
        <DialogContent dividers={scroll === 'paper'}>
          <DialogContentText className="add-supplier-product"
            id="scroll-dialog-description"
            ref={descriptionElementRef}
            tabIndex={-1}
          >
            <InputLabel className="popup-label">Item Code:</InputLabel>
            <TextField id="outlined-basic"  type="number" variant="outlined" size="small" name="contactNumber"
         />  
          <InputLabel className="popup-label">Product Name:</InputLabel>
          <Autocomplete id="combo-box-demo"  options={top100Films}  getOptionLabel={(option) => option.title} 
      renderInput={(params) => <TextField {...params} id="outlined-basic" size="small"  variant="outlined" />} />
          <InputLabel className="popup-label">Quantity:</InputLabel>
            <TextField id="outlined-basic"  type="number" variant="outlined" size="small" name="contactNumber"
         />  
          <InputLabel className="popup-label">Price:</InputLabel>
            <TextField id="outlined-basic"  type="number" variant="outlined" size="small" name="contactNumber"
         />  
          <InputLabel className="popup-label">Tax:</InputLabel>
            <TextField id="outlined-basic"  type="number" variant="outlined" size="small" name="contactNumber"
         />  
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
