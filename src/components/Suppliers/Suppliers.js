import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import '../Suppliers/Supplierstyle.css'
import Supplierpopup from '../Suppliers/supplier-popup'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import Addproducts from '../Suppliers/addproducts';

export default function Suppliers() {
  return (
    <div>
      <Paper>
        <Grid container spacing={3} >
          <Grid item md={8} xs={6} className="breadcrumbs">
            <Breadcrumbs aria-label="breadcrumb">
              <Link color="inherit" href="/" >Simple Purchase Orders</Link>
              {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
              <Typography color="textPrimary">Welcome</Typography>
            </Breadcrumbs>
          </Grid>
          <Grid item md={4} xs={6} className="breadcrumbs1">
            <a href="/supplierview" style={{ textDecoration: 'none' }}><Button variant="outlined" size="small">Blank PO</Button></a>
            <Button variant="outlined" size="small">Help</Button>
            <Button variant="contained" size="small" color="primary">Settings</Button>
          </Grid>
        </Grid>
      </Paper>
      <Grid container className="Breadcrumbs-head">
        <Grid md={8}>
        </Grid>
        <Grid md={2}>
          <Supplierpopup />
        </Grid>
        <Grid md={2}>
          <Addproducts />
        </Grid>
      </Grid>
      <Grid container style={{ padding: '12px!important' }}>
        <Paper style={{ width: '100%' }}>
          <TableContainer>
            <Table aria-label="simple table">
              <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
                <TableRow className="supplier-table">
                  <TableCell align="center" style={{ width: '20%' }}>Supplier Code</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Supplier Name</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Email</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Mobile Number</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow className="supplier-table1">
                  <TableCell align="center">12345</TableCell>
                  <TableCell align="center">New Express Media</TableCell>
                  <TableCell align="center">karbharihb@gmail.com</TableCell>
                  <TableCell align="center">9975578030</TableCell>
                  <TableCell align="center">
                    <Link to="/supplierview" style={{ textDecoration: 'none' }}><Button variant="contained" className="common-button" size="small">View</Button></Link>
                  </TableCell>
                </TableRow>
                <TableRow className="supplier-table1">
                  <TableCell align="center">12345</TableCell>
                  <TableCell align="center">SSK Incorporation</TableCell>
                  <TableCell align="center">karbharihb@gmail.com</TableCell>
                  <TableCell align="center">8888874477 </TableCell>
                  <TableCell align="center">
                    <Link to="/supplierview" style={{ textDecoration: 'none' }}><Button variant="contained" className="common-button" size="small">View</Button></Link>
                  </TableCell>
                </TableRow>
                <TableRow className="supplier-table1">
                  <TableCell align="center">12345</TableCell>
                  <TableCell align="center">Sri Vinayaga Traders </TableCell>
                  <TableCell align="center">karbharihb@gmail.com</TableCell>
                  <TableCell align="center">9842266407</TableCell>
                  <TableCell align="center">
                    <Link to="/supplierview" style={{ textDecoration: 'none' }}><Button variant="contained" className="common-button" size="small">View</Button></Link>
                  </TableCell>
                </TableRow>
                <TableRow className="supplier-table1">
                  <TableCell align="center">12345</TableCell>
                  <TableCell align="center">New Express Media</TableCell>
                  <TableCell align="center">karbharihb@gmail.com</TableCell>
                  <TableCell align="center">9975578030</TableCell>
                  <TableCell align="center">
                    <Link to="/supplierview" style={{ textDecoration: 'none' }}><Button variant="contained" className="common-button" size="small">View</Button></Link>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </Grid>
    </div>
  )
}
