import React from 'react'
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';
import {MuiPickersUtilsProvider,KeyboardDatePicker,} from '@material-ui/pickers';
import Grid from '@material-ui/core/Grid';
import '../Purchaseorder/purchase.css'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import HelpIcon from '@material-ui/icons/Help';
import CancelIcon from '@material-ui/icons/Cancel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});


export default function Createpo(props) {
    const [rows, setRows] = React.useState([]);
    const [value, setValue] = React.useState(0);
    const [quantityvalue, setQuantityvalue] = React.useState(0);
    const changeValue = (e) => {
        setValue(e.target.value);
      };
      const quantitychangeValue = (e) => {
        setQuantityvalue(e.target.value);
      };
      const total = value * quantityvalue;

  const handleAddRow = () => {
    let item = {
        id:rows.length+1,
    };
    setRows([...rows, item]);
  };
  const handleRemoveSpecificRow = item => () => {
   
    let items = rows.filter(row => row.id !== item.id);
    setRows(items);
  };
    const classes = useStyles();
    const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));
    const handleDateChange = (date) => {
        setSelectedDate(date);
    };
    return (
        <div>
               <Grid container>
       <Grid container spacing={3} >
        <Grid item xs={8} className="breadcrumbs">
          <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit" href="/" >Simple Purchase Orders</Link>
      {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
      <Typography color="textPrimary">Create PO</Typography>
    </Breadcrumbs>
        </Grid>
        <Grid item xs={4} className="breadcrumbs1">
        <Button variant="outlined" size="small">Settings</Button>
        <Button variant="outlined" size="small">Save Draft</Button>
        <Button  size="small" color="primary" variant="contained">Create PO</Button>
        </Grid>
      </Grid>
    </Grid>
            <Paper elevation={3} style={{margin:'10px'}}>
                <Box display="flex" justifyContent='space-around' className="createpo-head">
                    <Box width={1 / 5} ><label style={{ width: '75%' }}>PO Number </label>
                        <FormControl fullWidth variant="outlined" >
                            <OutlinedInput id="outlined-adornment-amount" className="createpo-input" size="small" />
                        </FormControl> </Box>
                    <Box width={1 / 5} ><label>Date</label>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} >
                            <Grid container justify="space-around" style={{ marginTop: '-15px' }}>
                                <KeyboardDatePicker variant="outlined" className="datepicker"
                                    margin="normal"
                                    id="date-picker-dialog"
                                    format="MM/dd/yyyy"
                                    inputVariant="outlined"
                                    defaultValue="Small" size="small"
                                    value={selectedDate}
                                    onChange={handleDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                /></Grid>
                        </MuiPickersUtilsProvider>
                    </Box>
                    <Box width={1 / 5}><label>Select Supplier</label>
                        <FormControl fullWidth variant="outlined" >
                            <OutlinedInput id="outlined-adornment-amount" className="createpo-input" size="small" />
                        </FormControl> </Box>
                    <Box width={1 / 5}><label style={{ width: '50%' }}>Due Date</label>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} >
                            <Grid container justify="space-around" style={{ marginTop: '-15px' }}>
                                <KeyboardDatePicker variant="outlined"
                                    margin="normal"
                                    id="date-picker-dialog"
                                    format="MM/dd/yyyy"
                                    inputVariant="outlined"
                                    defaultValue="Small" size="small"
                                    value={selectedDate}
                                    onChange={handleDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                /></Grid>
                        </MuiPickersUtilsProvider> </Box>

                </Box>
                <TableContainer component={Paper} >
                    <Table className={classes.table} aria-label="simple table" >
                        <TableHead>
                            <TableRow className="create-po-table" style={{ background: 'rgb(245, 245, 245)', }}>
                            <TableCell align="center" style={{ width: '5%' }}>S.No</TableCell>
                                <TableCell align="center" style={{ width: '40%' }}>Products</TableCell>
                                <TableCell align="center" style={{ width: '10%' }}>In Stock <HelpIcon size="small" color="primary" style={{ paddingTop: '4px' }} /></TableCell>
                                <TableCell align="center" style={{ width: '12%' }}>Quantity <HelpIcon size="small" color="primary" style={{ paddingTop: '4px' }} /></TableCell>
                                <TableCell align="center" style={{ width: '15%' }}>Cost</TableCell>
                                <TableCell align="center" style={{ width: '15%' }}>Line Total</TableCell>
                                <TableCell align="center" style={{ width: '10%' }}></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {rows.map((item, index) => (
                                <TableRow key={index}>
                                     <TableCell align="center">{item.id}</TableCell>
                                    <TableCell component="th" scope="row" align="center">Bussiness-focused clones Generic Frozen car</TableCell>
                                    <TableCell align="center">4</TableCell>
                                    <TableCell align="center"> <TextField required id="standard-required" type="number" variant="outlined" size="small" onChange={changeValue}/>
                                    </TableCell>
                                    <TableCell align="center"><TextField required id="standard-required" type="number" variant="outlined" size="small" onChange={quantitychangeValue}/></TableCell>
                                    <TableCell align="center">{total}</TableCell>
                                    <TableCell align="center"><CancelIcon color="secondary" onClick={handleRemoveSpecificRow(item)} /></TableCell>
                                </TableRow>
                            ))}
                            <TableRow className="create-po-table-total">
                                <TableCell align="right" colSpan="2">Total Quantity</TableCell>
                                <TableCell align="center">0</TableCell>
                                <TableCell align="center">Total Cost</TableCell>
                                <TableCell align="center"></TableCell>
                            </TableRow>
                            <TableRow className="create-po-table-total">
                                <TableCell align="center" colSpan="3">Shipping</TableCell>
                                <TableCell align="center"><TextField required id="standard-required" variant="outlined" size="small" defaultValue="0" /></TableCell>
                            </TableRow>
                            <TableRow className="create-po-table-total">
                                <TableCell align="center" colSpan="2"><TextareaAutosize aria-label="minimum height" rowsMin={9} placeholder="" style={{ width: '400px' }} /></TableCell>
                                <TableCell align="center" colSpan="4">
                                    <Button className="common-button createpo-addtax" variant="contained" size="small">Add tax</Button> 
                                    <Button variant="contained" className="common-button createpo-addtax" size="small">Add Blank row</Button>
                                    <Button variant="contained" className="common-button createpo-addtax" size="small" onClick={handleAddRow}>Add Products</Button>
                                    </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>
    )
}
