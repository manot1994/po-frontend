import React from 'react'
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import '../Invoice/Invoicestyle.css'
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';


export default function Orderdetail() {
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Grid container>
     
      <Grid container spacing={3} >
        <Grid item sm={9} xs={12} className="breadcrumbs">
          <Breadcrumbs aria-label="breadcrumb">
            <Link color="inherit" href="/" >Simple Purchase Orders</Link>
            {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
            <Typography color="textPrimary">Welcome</Typography>
          </Breadcrumbs>
        </Grid>
        <Grid item xs={3} className="breadcrumbs1">
          <Button variant="outlined" size="small">Resend PO</Button>
          <a href="/createpo"><Button variant="contained" size="small" color="primary">Edit</Button></a>
        </Grid>
      </Grid>
     
      <div style={{ width: '100%', margin: '12px' }}>
        <Paper>
        <Grid container spacing={3} style={{marginBottom:'2px'}}>
        <Grid item xs={12} align="center" fontWeight="fontWeightBold">Invoice Details</Grid>
      </Grid>
      <hr className="hr-line"/>
          <Grid container spacing={3} style={{textAlign:'center', marginBottom:'10px'}}>
<Grid item md={4} xs={6}>Invoice Number: 1030</Grid>
<Grid item md={4} xs={6}>Date: 2020-04-20</Grid>
<Grid item md={4} xs={12}><ButtonGroup size="small" aria-label="small outlined button group">
                  <Button className="common-button">Mark Completed</Button>
                  <Button className="common-button">Transfer All</Button>
                  <Button>Delete</Button>
                </ButtonGroup>
                </Grid>
</Grid>


          <TableContainer >
            <Table aria-label="simple table">
              <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
                <TableRow className="orderdetails-table">
                  <TableCell align="center" style={{ width: '35%' }}>Item</TableCell>
                  <TableCell align="center" style={{ width: '30%' }}>Quantity</TableCell>
                  <TableCell align="center" style={{ width: '35%' }}>Transfer</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow className="orderdetails-table1">
                  <TableCell align="center">12T5KI - 12 Ti Xelium Ski"s - 163cm</TableCell>
                  <TableCell align="center">1</TableCell>
                  <TableCell align="center" variant="outlined" color="primary" onClick={handleClickOpen}>Update Inventory</TableCell>
                </TableRow>

              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>

      {/* Popup Modal */}
      <div>

        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Are you sure you want to transfer this?"}</DialogTitle>
          <hr />
          <DialogContent>
            <DialogContentText>
              This will transfer of 12T5KI - 12 Ti Xelium Ski"s - 163cm
          </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleClose} variant="contained" size="small">Cancel</Button>
            <Button onClick={handleClose} color="primary" variant="contained" size="small" autoFocus>Transfer</Button>
          </DialogActions>
        </Dialog>
      </div>
    </Grid>
  )
}
