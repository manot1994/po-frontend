import React, { useState, useEffect} from 'react'
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import {Link} from "react-router-dom";
import LoginPage from '../pages/Login';
import axios from 'axios';
import { useForm } from "react-hook-form";

const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
    },
    image: {
      backgroundImage: 'url(https://cdn2.hubspot.net/hubfs/4483341/Imported_Blog_Media/shutterstock_252243379-1024x683.jpg)',
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    
  }));
export default function Supplierlogin( {Login, error}) {
    const classes = useStyles();
    const[details, setDetails]=useState({email:"", password:""});
    const[response, setResponse]=useState({data:"", status:""});
    console.log(response, "mano")
    const { handleSubmit } = useForm();

    const onSubmit = (e)=>{
        
      let params={
        "email":details.email,
        "password":details.password
      };
    
      axios.post("http://45.79.120.10:3445/login_supplier", params)
        .then(res => setResponse({
          data: res.data,
          status: res.status
        })).catch((err)=>{
          console.log("failed")
        })
        // history.push('/profile')

    }
   
    return (
        <div>
          {(response.status == 200) ? (
               <div>test</div>
        ):(
          <Grid container component="main" className={classes.root}>
      {/* <CssBaseline /> */}
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
           <img alt="" src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png"></img>
            {/* <Typography component="h1" variant="h5">
              Sign in
            </Typography> */}
            <form className={classes.form} noValidate onSubmit={handleSubmit(onSubmit)}>
              {response.data}
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus onChange={e => setDetails({...details, email:e.target.value})} value={details.email}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={e => setDetails({...details, password:e.target.value})} value={details.password}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit} value={details}
            >
              Sign In
            </Button>
            {/* <input type="submit" value={details}></input> */}
             
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
                  </Link>
                </Grid>
                {/* <Grid item>
                  <Link href="#" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid> */}
              </Grid>
              {/* <Box mt={5}>
                <Copyright />
              </Box> */}
            </form>
             </div>
      </Grid>
    </Grid>
         )}
       
        </div>
    )
}
